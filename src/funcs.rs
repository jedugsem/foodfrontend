use crate::auth::request_get;
use crate::auth::request_post;
use crate::types::*;
use gloo::console::console_dbg;
//use reqwasm::http::Request;
use reqwest::Request;
use reqwest::Client;
use serde::de::DeserializeOwned;
use crate::consts::API;
pub async fn post_submit(
    name: String,
    image: String,
    price: String,
    stock: String,
    desc: String,
    unit: String,
) -> Result<String, ()> {
    let response = Client::new().post(&format!(
        "{}data/submit?name={}&desc={}&stock={}&price={}&unit={}&image={}",
        API,name, desc, stock, price, unit, image
    ))
    .send()
    .await;
    console_dbg!("wichtig{:?}", name);
    if let Ok(result) = response {
        let result = result.text().await.unwrap();
        if result == "ok" {
            Err(())
        } else {
            Ok(result.to_string())
        }
    } else {
        Ok("response".to_string())
    }
}
pub async fn fetch_balance(url: String, mode: i32, id: Option<i32>) -> Result<BalanceList, String> {
    if id.is_some() {
        console_dbg!("run");
        let req = request_get::<Option<BalanceList>>(format!(
            "{}?id={}&history={}",
            url,
            id.unwrap(),
            mode
        ))
        .await;
        if let Ok(Some(data)) = req {
            Ok(data)
        } else {
            console_dbg!("error balance");
            Err("dodsd".to_string())
        }
    } else {
        console_dbg!("else");
        Err("id false".to_string())
    }
}
pub async fn _fetch_user(url: String) -> Result<UserSend, String> {
    fetch::<UserSend>(url).await
}
pub async fn fetch_db(url: String) -> Result<ProDb, String> {
    if let Ok(DataWrap::ProDb(db)) = fetch::<DataWrap>(url).await {
        Ok(db)
    } else {
        Err("trash-db".to_string())
    }
}
pub async fn post_buy(user: Option<UserSend>, id: i32, input: String) -> Result<String, ()> {
    let input = input.parse::<f32>();
    if user.is_some() && input.is_ok() {
        let response = Client::new().post(&format!(
            "{}data/buy?pro={}&user={}&q={}",
            API,
            id,
            user.unwrap().id,
            input.unwrap()
        ))
        .send()
        .await;
        if let Ok(result) = response {
            let result = result.text().await.unwrap();
            if result == "ok" {
                Err(())
            } else {
                Ok(result.to_string())
            }
        } else {
            Ok("response".to_string())
        }
    } else {
        Ok("parse or user error".to_string())
    }
}
pub async fn post_user(url: String, name: String, balance: String,password:String,salt:String) -> Result<String, ()> {
    let balance = balance.parse::<i32>();
    console_dbg!("post_balance");
    if balance.is_ok() {
        console_dbg!("post_user");
        let body = format!("{} {} {} {}",name,balance.unwrap(),password,salt);
        //let response = request_post::<String,String>(url,body).await;
        //let response = Request::post(&format!( "{}?name={}&balance={}", url, name, balance.unwrap())) .send() .await;
        let response = Client::new().post(url).body(body) .send() .await;

        if let Ok(result) = response {
            let result = result.text().await.unwrap();
            if result == "ok" {
                Err(())
            } else {
                Ok(result.to_string())
            }
        } else {
            Ok("response".to_string())
        }
    } else {
        Ok("parse or user error".to_string())
    }
}
pub async fn post_login(name: String, password: String) -> Result<(String, UserSend), String> {
    let body = format!("{} {}", name, password);
    let resp = Client::new()
        .post(&format!("{}login",API))
        .body(body)
        .send()
        .await;

    if let Ok(result) = resp {
        if let Ok(bin) = result.bytes().await {
            //Ok(repo)

            let decoded: Result<Login, Box<bincode::ErrorKind>> = bincode::deserialize(&bin[..]);
            if let Ok(data) = decoded {
                console_dbg!("data ok");
                Ok((data.token, data.user))
            } else {
                let string: Result<String, Box<bincode::ErrorKind>> =
                    bincode::deserialize(&bin[..]);
                if let Ok(str) = string {
                    Err(str)
                } else {
                    Err("serde-error".to_string())
                }
            }
        } else {
            console_dbg!("tre ok");
            Err("bin-error".to_string())
        }

    //            let result = result.text().await;
    //            if result.is_ok() {
    //                Ok((result.unwrap(),UserSend{id : 0, username:"manu".to_string(), balance : 0}))
    //            } else {
    //                Err("error".to_string())
    //            }
    } else {
        Err("response".to_string())
    }
}

pub async fn fetch<T>(url: String) -> Result<T, String>
where
    T: DeserializeOwned,
{
    let response = Client::new().get(&url).send().await;
    if let Ok(resp) = response {
        if let Ok(bin) = resp.bytes().await {
            //Ok(repo)

            let decoded: Result<Option<T>, Box<bincode::ErrorKind>> =
                bincode::deserialize(&bin[..]);
            if let Ok(Some(data)) = decoded {
                console_dbg!("data ok");
                Ok(data)
            } else {
                console_dbg!("smash ok");
                Err("serde-error".to_string())
            }
        } else {
            console_dbg!("tre ok");
            Err("bin-error".to_string())
        }
    } else {
        console_dbg!("wierd ok");
        Err("req-error".to_string())
    }
}
async fn _send_submit() {}

pub async fn _fetch_west<T>(url: String) -> Result<T, String>
where
    T: DeserializeOwned,
{
    let token = "passs";
    let response = Client::new().get(&url).bearer_auth(token).send().await;
    if let Ok(resp) = response {
        if let Ok(bin) = resp.bytes().await {
            //Ok(repo)

            let decoded: Result<Option<T>, Box<bincode::ErrorKind>> =
                bincode::deserialize(&bin[..]);
            if let Ok(Some(data)) = decoded {
                console_dbg!("data ok");
                Ok(data)
            } else {
                console_dbg!("smash ok");
                Err("serde-error".to_string())
            }
        } else {
            console_dbg!("tre ok");
            Err("bin-error".to_string())
        }
    } else {
        console_dbg!("wierd ok");
        Err("req-error".to_string())
    }
}
