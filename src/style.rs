use stylist::style;
use stylist::Style;

pub fn center() -> Style {
    style!(
        r#"display: flex;
        align-items: center;
        justify-content: center;
           "#
    )
    .expect("center failed")
}
pub fn vcenter() -> Style {
    style!(
        r#"
        display: flex;
        justify-content: center;
           "#
    )
    .expect("vcenter failed")
}
pub fn tile() -> Style {
    style!(
        r#"
background-color : #808080;
border-radius: 4px;
width : 150px;
height : 150px;
           "#
    )
    .expect("tile failed")
}

pub fn white() -> Style {
    style!(
        r#"
    background-color: white;
           "#
    )
    .expect("white error")
}
pub fn button() -> Style {
    style!(
        r#"
     color: white;
       margin: 15px;
       padding: 5px;
       border-radius:4px;
       border: 2px solid black;
       background-color : #808080;
           "#
    )
    .expect("white error")
}
pub fn app() -> Style {
    style!(
        r#"
 width: 90%;
   display: grid;
   grid-gap: 15px;
     justify-content: center;
   /*overflow: hidden;*/
 /*   grid-template-columns: repeat(auto-fill, 200px); */
   grid-template-columns: repeat(auto-fit, 150px);
           "#
    )
    .expect("white error")
}
