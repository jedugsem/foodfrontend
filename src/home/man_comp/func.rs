use bounce::prelude::*;
use gloo::console::console_dbg;
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement};
use yew::prelude::*;
use yew_hooks::use_async;

use crate::funcs::*;
use crate::types::*;
use crate::ucomps::Modal;

use crate::consts::API;

#[function_component(Konto)]
pub fn balance() -> Html {
    let token = use_atom::<Token>();
    let user = use_atom::<User>();
    let db_han = use_atom::<Db>();
    let id = if &user.0.is_some() == &true {
        Some(user.0.clone().unwrap().id)
    } else {
        None
    };
    let state = {
        let id = id.clone();
        use_async(async move {
            fetch_balance(format!("{}auth/balance",API), 0, id).await
        })
    };

    let onclick = {
        let state = state.clone();
        move |_| {
            console_dbg!("token:{}", &(*token).0);
            state.run();
        }
    };

    //let vector = state.data.unwrap();
    let db = (*db_han).clone().0.unwrap();
    let details = if state.data.is_some() {
        console_dbg!("data is some");
        let data = &state.data.clone().unwrap();
        data.vec
            .iter()
            .map(|trans| {
                let pro: Vec<Produkt> = db
                    .vec
                    .clone()
                    .into_iter()
                    .filter(|pro| pro.id == trans.pro_id)
                    .collect();
                let pro = pro[0].clone();
                html!(

                    <TransDetails pro={pro} trans={trans.clone()}/>

                )
            })
            .collect::<Html>()
    } else {
        html!(<p>{""}</p>)
    };

    html!(
          <p>
          <button onclick={onclick}>{"banlance"}</button>
          <p>
          {
                if let Some(data) = &state.data {
                    format!("Balance : {} ct",data.balance.balance)
                } else {
                    format!("")
                }
          }
        </p>
        <p><table>
                    <tr>
                        <th>{"Name"}</th>
                        <th>{"rprice"}</th>
                        <th>{"Quanti"}</th>
                        <th>{"Unit"}</th>
                        <th>{"aprice"}</th>
                    </tr>

                    {details}
    </table></p>
        </p>
          )
}

#[derive(Clone, Properties, PartialEq)]
pub struct KontoDetailsProps {
    pub trans: Trans,
    pub pro: Produkt,
}

#[function_component(TransDetails)]
pub fn pro_details(
    KontoDetailsProps {
        trans: i,
        pro: produkt,
    }: &KontoDetailsProps,
) -> Html {
    console_dbg!("{}", i.clone());
    html! {
            <tr>
                    <th>
                   {produkt.pro.clone()}
                   </th>
                     <th>
                   {produkt.price}
                   </th>

                          <th>
                   {i.quantity}
                   </th>
                   <th>
                   {format!("{:?}",produkt.mesure)}
                   </th>

    <th>
               {i.end_price}
                   </th>


                          // <th>{format!("|{}|{}|{:?}|{}|",i.pro_id,i.quantity,i.unit,i.end_price)}</th>
            </tr>
        }
}

#[function_component(AddUser)]
pub fn add_user() -> Html {
    let name_handle = use_state(String::default);
    let name = (*name_handle).clone();
    let balance_handle = use_state(String::default);
    let balance = (*balance_handle).clone();
    let salt_handle = use_state(String::default);
    let salt = (*salt_handle).clone();
    let password_handle = use_state(String::default);
    let password = (*password_handle).clone();
    let state = {
        //let id = id.clone();
        let name = name.clone();
        let balance = balance.clone();
        let password = password.clone();
        let salt = salt.clone();
        use_async(async move {
            post_user(
                format!("{}data/adduser",API),
                name,
                balance,
                password,
                salt,
            )
            .await
        })
    };

    let onclick = {
        let state = state.clone();
        move |_| {
            state.run();
        }
    };

    let on_input1 = {
        let name_handle = name_handle.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from chil    d
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                name_handle.set(input.value());
            }
        })
    };
    let on_input2 = {
        let balance_handle = balance_handle.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from chil    d
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                balance_handle.set(input.value());
            }
        })
    };
   let on_input3 = {
        let salt_handle = salt_handle.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from chil    d
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                salt_handle.set(input.value());
            }
        })
    };


    let on_input_pass = {
        let password_handle = password_handle.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from chil    d
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                password_handle.set(input.value());
            }
        })
    };

    html!(
      <p>
      <Modal string={"add user".to_string()} classes={classes!()} >
      <p>{"Modal"}</p>
            <p>
                <label for="name">
                     { "Name :          " }
                      <input oninput={on_input1}
                          id="name"
                           type="text"
                           value={name.clone()}
                       />

               </label>
            </p>
             <p>
                <label for="balance">
                     { "Balance :          " }
                      <input oninput={on_input2}
                          id="balance"
                           type="text"
                           value={balance.clone()}
                       />

               </label>
            </p>
             <p>
                <label for="salt">
                     { "Emial :          " }
                      <input oninput={on_input3}
                          id="salt"
                           type="text"
                           value={salt.clone()}
                       />

               </label>
            </p>

             <p>
                <label for="password">
                     { "Password :          " }
                      <input oninput={on_input_pass}
                          id="password"
                           type="password"
                           value={password.clone()}
                       />

               </label>
            </p>

           <p>{if let Some(data) = &state.data {
                 format!("{}",data)
             } else {
                 "worked".to_string()
             }}</p>
            <button onclick={onclick}>{"Add"}</button>
      </Modal>
    </p>
      )
}
