use bounce::prelude::*;

use stylist::yew::styled_component;
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement};
use yew::prelude::*;
use yew_hooks::use_async;

use crate::funcs::*;
use crate::types::*;

#[styled_component(Login)]
pub fn login() -> Html {
    html! {
        <div>
            <p class={crate::style::vcenter()}><b>{"Login"}</b></p>
            <SelectUser/>
        </div>
    }
}

#[derive(Properties, PartialEq)]
struct SelectProps {
    pub on: Callback<UserSend>,
}

#[function_component(SelectUser)]
fn user() -> Html {
    let base_url = "http://127.0.0.1:5000/data/user?name=";
    let url = use_state(|| "".to_string());
    let input_value_handle = use_state(String::default);
    let input_value = (*input_value_handle).clone();

    let state = {
        let url = url.clone();
        use_async(async move { fetch_user((*url).clone()).await })
    };

    let on_cautious_change = {
        let input_value_handle = input_value_handle.clone();
        let url = url.clone();
        let state = state.clone();
        Callback::from(move |e: Event| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                url.set(format!("{}{}", base_url, input.value()));
                state.run();
                input_value_handle.set(input.value());
            }
        })
    };

    let usr = use_atom::<User>();
    if state.data.is_some() {
        usr.set(User(state.data.clone()));
    }
    //let result = "Username: Julia".to_string();

    html!(
    <div>
        <p class={crate::style::vcenter()}>
        <label for="user-sel">
            { "Username:" }
            <input onchange={on_cautious_change}
                id="user-sel"
                type="text"
                value={input_value.clone()}
            />
        </label>
        </p>
        <p class={crate::style::vcenter()}>

            if let Some(usr) = &state.data {
                <p>{ "Username: " }<b>{ &usr.username }</b></p>
            }
        </p>
    </div>
        )
}
