use bounce::prelude::*;
use gloo::console::console_dbg;
use stylist::style;
use stylist::yew::styled_component;
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement};
use yew::prelude::*;
use yew_hooks::use_async;

use crate::funcs::*;
use crate::types::*;
use crate::ucomps::Modal;

#[derive(Clone, Properties, PartialEq)]
pub struct ProDetailsProps {
    pub pro: Produkt,
}

#[function_component(ProDetails)]
pub fn pro_details(ProDetailsProps { pro }: &ProDetailsProps) -> Html {
    html! {
        <div>
            <h3>{ pro.pro.clone() }</h3>
        </div>
    }
}

#[derive(Properties, PartialEq)]
pub struct ProduktListProps {
    pub prodb: ProDb,
    pub on_click: Callback<Produkt>,
}

#[styled_component(ProduktList)]
pub fn produkt_list(ProduktListProps { prodb, on_click }: &ProduktListProps) -> Html {
    let on_click = on_click.clone();
    let desc = style!(
        "overflow : scroll;
        height : 40px;"
    )
    .expect("yo crazy yo");
    let user = use_atom::<User>();

    prodb
        .vec
        .iter()
        .map(|pro| {
            let input_value_handle = use_state(String::default);
            let input_value = (*input_value_handle).clone();

            let on_pro_select = {
                let on_click = on_click.clone();
                let pro = pro.clone();
                Callback::from(move |_| on_click.emit(pro.clone()))
            };

            let buy = {
                let input_value = input_value.clone();
                let pro = pro.clone();
                let user = user.0.clone();
                use_async(async move { post_buy(user, pro.id.clone(), input_value.clone()).await })
            };

            let onclick = {
                let user = user.clone();
                let id = pro.id.clone();
                let input = input_value.clone();
                let buy = buy.clone();
                Callback::from(move |_| {
                    console_dbg!("{}", input.clone());
                    buy.run();
                    console_dbg!("{}", id.clone());
                    if let Some(user) = &user.clone().0 {
                        console_dbg!("{}", &user.username);
                    }
                })
            };

            let on_input = {
                let input_value_handle = input_value_handle.clone();

                Callback::from(move |e: InputEvent| {
                    // When events are created the target is undefined, it's only
                    // when dispatched does the target get added.
                    let target: Option<EventTarget> = e.target();
                    // Events can bubble so this listener might catch events from child
                    // elements which are not of type HtmlInputElement
                    let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

                    if let Some(input) = input {
                        input_value_handle.set(input.value());
                    }
                })
            };

            html! {

                <div style="text-align:center" class={crate::style::tile()}
                //class={classes!("grey","big-corner","pro-tile","item")}
                onclick={on_pro_select}>
                <p>
                {format!("{}", pro.price)}
                </p>

                <p>
                {format!("{}", pro.stock)}
                </p>
                <p class={desc.clone()}>
                {format!("{}", pro.desc)}
                </p>
                <p>
                <Modal string={pro.pro.clone()} classes={classes!()}>
                    <p>{pro.pro.clone()}</p>

                    <label for="cautious-input">
                         { "My cautious input:" }
                         <input oninput={on_input}
                             id="cautious-input"
                             type="text"
                             value={input_value.clone()}
                         />
                     <p>{ input_value.clone()}</p>
                     <p>{ input_value.len()}</p>
                 </label>

                    <p>{format!("{:?}",pro.mesure)}</p>
                    <p>{if let Some(data) = buy.data.clone() {
                                format!("{}",data)
                        }else {"".to_string()}
                    }</p>
                    <button onclick={onclick}>{"buy"}</button>
                </Modal>
                </p>
                </div>

            }
        })
        .collect()
}
