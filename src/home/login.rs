use bounce::prelude::*;

use stylist::yew::styled_component;
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement};
use yew::prelude::*;
use yew_hooks::use_async;

use crate::auth::set_token;
use crate::funcs::*;
use crate::types::*;

#[styled_component(LoginTok)]
pub fn login_tok() -> Html {
    html! {
        <div>
            <p class={crate::style::vcenter()}><b>{"Login"}</b></p>
            <SelectUser/>
        </div>
    }
}

#[derive(Properties, PartialEq)]
struct SelectProps {
    pub on: Callback<UserSend>,
}

#[function_component(SelectUser)]
fn user() -> Html {
    //let input_value_handle = use_state(String::default);
    //let input_value = (*input_value_handle).clone();
    let token = use_atom::<Token>();
    let name_handle = use_state(|| "".to_string());
    let name = (*name_handle).clone();
    let password_handle = use_state(|| "".to_string());
    let password = (*password_handle).clone();

    let state = {
        let name = name.clone();
        let password = password.clone();
        use_async(async move { post_login(name, password).await })
    };

    let on_cautious_name = {
        let name_handle = name_handle.clone();
        Callback::from(move |e: Event| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                name_handle.set(input.value());
            }
        })
    };
    let on_cautious_pass = {
        let password_handle = password_handle.clone();
        Callback::from(move |e: Event| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                password_handle.set(input.value());
            }
        })
    };
    let onclick = {
        let state = state.clone();

        Callback::from(move |_| {
            state.run();
        })
    };
    let usr = use_atom::<User>();
    if state.data.is_some() {
        usr.set(User(Some(state.data.as_ref().unwrap().1.clone())));
        token.set(Token(state.data.as_ref().unwrap().0.clone()));
        set_token(Some(state.data.as_ref().unwrap().0.clone()));
    }
    //let result = "Username: Julia".to_string();

    html!(
    <div>


        <p class={crate::style::vcenter()}>
        <label for="user-sel">
            { "Username:" }
            <input onchange={on_cautious_name}
                id="user-sel"
                type="text"
                value={name.clone()}
            />
        </label>
        </p>

        <p class={crate::style::vcenter()}>
        <label for="pass">
            { "Password:" }
            <input onchange={on_cautious_pass}
                id="pass"
                type="password"
                value={password.clone()}
            />
        </label>
        </p>
        <p class={crate::style::vcenter()}>

            if let Some(usr) = &state.data {
                <p>{ "Succeful Login: " }<b>{ &usr.1.username }</b></p>
            }
        </p>
        <p class={crate::style::vcenter()}>
        <button onclick={onclick}>{"Login"}</button>
        </p>
    </div>
        )
}
