//use crate::funcs::post_login;
use crate::home::man_comp::func::*;
use crate::ucomps::*;
use gloo::console::console_dbg;
//use reqwasm::http::Request;
//use reqwest::Client;
//use wasm_bindgen::JsCast;
use reqwest::Client;
use wasm_bindgen_futures::spawn_local;
use yew::prelude::*;
use crate::consts::API;
pub enum Msg {
    CheckDb,
    ResetDb,
    _Submit,
    Buy,
    DeleteDb,
}

pub struct Manage {
    // props: Props
//socket : &'static (SplitSink<WebSocket, Message>, SplitStream<WebSocket>),
}

impl Component for Manage {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {} // { value: 0, prodb: Rc::default() , user : None }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::CheckDb => {
                spawn_local(async move {
                    //.header
                    //username:password@
                    let resp = Client::new().post(&format!("{}setup?com=checkdb",API)).send().await;

                    //                    let body = format!("{} {}",user,pass);
                    //                    let resp = Client::new().post("http://127.0.0.1:5000/login").body(body)
                    //                        .send()
                    //                        .await;
                    //                    console_dbg!("send");
                                        if let Ok(code) = resp {
                                            console_dbg!("{}", code.text().await.unwrap());
                                        }
                });
                true
            }
            Msg::ResetDb => {
                spawn_local(async move {
                    //let token = "wooha";
                    let resp = Client::new().post(&format!("{}setup?com=fill",API)).send().await;
                    //let resp = Client::new()
                    //    .post("http://127.0.0.1:5000/setup?com=fill") //.bearer_auth(token)
                    //    .send()
                    //    .await;

                    if let Ok(code) = resp {
                        console_dbg!("{}", code.text().await.unwrap());
                    }
                });
                true
            }
            Msg::DeleteDb => {
                spawn_local(async move {
                    let resp = Client::new().post(&format!("{}setup?com=delete",API))
                        .send()
                        .await;
                    if let Ok(code) = resp {
                        console_dbg!("{}", code.text().await.unwrap());
                    }
                });

                console_dbg!("sjesa");

                true
            }
            Msg::_Submit => true,
            Msg::Buy => {
                spawn_local(async move {
                    let resp = Client::new().post(
                        &format!("{}data/buy?pro=1&q=500&user=1&price=140",API)
                    )
                    .send()
                    .await;
                    if let Ok(code) = resp {
                        console_dbg!("{}", code.text().await.unwrap());
                    }
                });
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        // This gives us a component's "`Scope`" which allows us to send messages, etc to the component.
        let link = ctx.link();

        html! {
            <>
            <div>
                <p>{ format!("Mangment button") }</p>
                <s>
                <button onclick={link.callback(|_| Msg::DeleteDb)}>{ "delete-db" }</button>
                <button onclick={link.callback(|_| Msg::ResetDb)}>{ "fill-db" }</button>
                <button onclick={link.callback(|_| Msg::CheckDb)}>{ "check-db" }</button>
                </s>
                //<p>{ format!("get data structures")} </p>

                <p> {   format!("new functions")   }</p>
                <s>
                <Submit/>
                <button onclick={link.callback(|_| Msg::Buy)}>{ "buy-test" }</button>
                </s>
                <div>
                <Konto/>
                </div>
                <div>
                <AddUser/>
                </div>
                <p>{ "yo" }</p>
            </div>
            </>
        }
    }
}
