use bounce::prelude::*;

use wasm_bindgen_futures::spawn_local;
use yew::prelude::*;

use yew_router::prelude::*;

//use crate::consts::*;
use crate::funcs::*;
use crate::types::*;
use crate::Route;

pub mod center;
pub mod debug;
//pub mod login;
pub mod login;
pub mod man_comp;
pub mod manage;

use crate::style::*;
use center::*;
use debug::*;
//use login::*;
use login::*;
use manage::*;
use crate::consts::{ROOT,API};
#[function_component(Home)]
pub fn home_html() -> Html {
    let user = use_atom::<User>();
    let prodb = use_atom::<Db>();
    {
        let prodb = prodb.clone();
        use_effect_with_deps(
            move |_| {
                spawn_local(async move {
                    if let Ok(db) = fetch_db(format!("{}data/db",API)).await {
                        //console_dbg!("new-product{}", &db.vec[0].pro);
                        prodb.set(Db(Some(db)));
                    }
                });

                || ()
            },
            (),
        );
    }

    //    let navigator = use_navigator().unwrap();
    let navigator = use_history().unwrap();

    let user_send = (*user).clone();
    let prodb_send = (*prodb).clone();
    let mut succes: (bool, bool) = (true, false);

    let mut user = UserSend {
        id: 0,
        username: "Error".to_string(),
        balance: 0,
    };
    let mut prodb = ProDb { vec: Vec::new() };

    if prodb_send.0.is_some() {
        succes.0 = true;
        prodb = prodb_send.0.unwrap()
    }
    if user_send.0.is_some() {
        succes.1 = true;
        user = user_send.0.unwrap();
    }
    let selected_pro = use_state(|| None);

    let on_pro_select = {
        let selected_pro = selected_pro.clone();
        Callback::from(move |pro: Produkt| selected_pro.set(Some(pro)))
    };

    let state = use_state(|| 1);

    let onclick = Callback::from(move |_| navigator.push(Route::Login));
    let onclick1 = {
        let state = state.clone();
        Callback::from(move |_| state.set(1))
    };
    let onclick2 = {
        let state = state.clone();
        Callback::from(move |_| state.set(2))
    };
    let onclick3 = {
        let state = state.clone();
        Callback::from(move |_| state.set(3))
    };
    let onclick4 = {
        let state = state.clone();
        Callback::from(move |_| state.set(4))
    };

    let details = selected_pro.as_ref().map(|pro| {
        html! {
            <ProDetails pro={pro.clone()} />
        }
    });
    html! {
    <div class={white()}>
        <div class={center()}>
            <button {onclick} class={button()}>{ "Setup" }</button>
            <button onclick={onclick1} class={button()}>{ "Login" }</button>
            <button onclick={onclick2} class={button()}>{ "Home" }</button>
            <button onclick={onclick3} class={button()}>{ "Manage" }</button>
            <button onclick={onclick4} class={button()}>{ "Debug" }</button>
        </div>
            <p>
                if (*state) == 1 {
                    <LoginTok/>
                } else if (*state) == 2{
                    <p>
                        if succes.0 {
                            <div class={center()}>
                                <div class={app()}>
                                    <ProduktList prodb={prodb} on_click={on_pro_select.clone()} />
                                </div>
                            </div>
                            <div>
                                { for details }
                            </div>
                       }
                    </p>
                } else if (*state) ==3 {
                    <p> <Manage /> </p>
                } else if (*state) == 4 {
                    <h1>{ format!("Debug") }</h1>
                    <Debug/>
                    <p>{ if succes.1 {format!("{}",user.username)}else{"None".to_string()}}</p>
                    <p>{ if succes.1 {format!("{}",user.id)}else{"None".to_string()}}</p>
                } else {
                    <p>{"Error"}</p>
                }
            </p>
        <div class={center()}>
            <p class={white()}>{ "Base" }</p>
            <p>
                <a href={ROOT} target="_blank">{ "localhost" }</a>
            </p>
        </div>


    </div>
           }
}
