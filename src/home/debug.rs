use wasm_bindgen::JsCast;

use web_sys::{EventTarget, HtmlInputElement};
use yew::prelude::*;
//use yew_feather::camera::Camera;

use crate::ucomps::Modal;
use stylist::style;
use stylist::yew::styled_component;

#[styled_component(Debug)]
pub fn debug() -> Html {
    let input_value_handle = use_state(String::default);
    let input_value = (*input_value_handle).clone();

    let on_cautious_change = {
        let input_value_handle = input_value_handle.clone();

        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                input_value_handle.set(input.value());
            }
        })
    };

    let right = style!(
        "
        color : red;

         "
    )
    .expect("");

    html!(
        <>
        <div class={crate::style::vcenter()}>
        <label for="cautious-input">
                        { "My cautious input:" }
                        <input oninput={on_cautious_change}
                            id="cautious-input"
                            type="text"
                            value={input_value.clone()}
                        />
                    <p>{ input_value.clone()}</p>
                    <p>{ input_value.len()}</p>
                </label>
 //               <Camera color="red" size="48" />
            </div>
            <Modal string={"buy"} classes={classes!()} >
                <p class={right}>{"test Text"}</p>
            </Modal>
        </>
    )
}
