//use bounce::prelude::*;
use gloo::console::console_dbg;
use reqwest::Client;
//use reqwasm::http::Request;
//use stylist::style;
use stylist::yew::styled_component;
use wasm_bindgen_futures::spawn_local;
use yew::prelude::*;
use yew_router::prelude::*;
use crate::consts::API;
//use crate::types::Token;
use crate::Route;
#[styled_component(Login)]
pub fn login() -> Html {
    //let navigator = use_navigator().unwrap();
    let navigator = use_history().unwrap();
    let onclick = Callback::from(|_| {
        spawn_local(async move {
            let resp = Client::new().post(&format!("{}setup?com=fill",API))
                .send()
                .await;

            if let Ok(code) = resp {
                console_dbg!("{}", code.text().await.unwrap());
            }
        })
    });
    let onclick_home = Callback::from(move |_| navigator.push(Route::Home));

    html! {<div>
    <button onclick={onclick}>{"Fill-db"}</button>
    <button onclick={onclick_home}>{"Home"}</button>



        <div class={crate::style::center()}>
    <h1>{"playground"}</h1>
    </div>
    </div>}
}
