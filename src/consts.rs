pub const _BUTTON: [&str; 6] = [
    "sbutton",
    "small-padding",
    "small-border",
    "grey",
    "f-black",
    "big-corner",
];
//pub const API : &str = "http://127.0.0.1:5003/";
//pub const ROOT : &str = "http://127.0.0.1:8080/";
pub const API : &str = "https://git.baum.uno:5000/";
pub const ROOT : &str = "https://web.baum.uno/";
