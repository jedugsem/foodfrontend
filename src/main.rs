use yew::prelude::*;
//use yew::use_effect_with_deps;
//use yew::{function_component, html, use_state, Callback, Html,classes};
//use reqwasm::{http::Request,Error};
//use serde::{de::DeserializeOwned, Deserialize, Serialize};
//use wasm_bindgen::JsCast;
//use web_sys::{EventTarget, HtmlInputElement};
//use yew_hooks::{use_async, use_async_with_options, UseAsyncOptions};
//use wasm_bindgen_futures::spawn_local;
//use bounce::prelude::*;
use bounce::BounceRoot;
//use gloo::console::*;
//use gloo::dialogs::*;

use yew_router::prelude::*;

mod auth;
mod consts;
mod error;
mod funcs;
mod style;
mod types;
mod ucomps;
//use types::*;
//use funcs::*;

mod login;
use login::login::*;
mod home;
use home::*;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Login,
    #[at("/home")]
    Home,
    #[not_found]
    #[at("/404")]
    NotFound,
}

fn switch(routes: &Route) -> Html {
    match routes {
        Route::Login => html! { <h1><Login/></h1> },
        Route::Home => html! {
                   <BounceRoot>
            <Home/>
        </BounceRoot>
        },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
    }
}

#[function_component(Main)]
fn app() -> Html {
    html! {

        <div>
        <BrowserRouter >
            <Switch<Route> render={Switch::render(switch)} />
        </BrowserRouter>
        </div>
    }
}

fn main() {
    yew::start_app::<Main>();
    //yew::Renderer::<Main>::new().render();
}
