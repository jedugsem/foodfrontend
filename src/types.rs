use bounce::prelude::*;
use serde::{Deserialize, Serialize};

use std::collections::HashMap;

/// Conduit api error info for Unprocessable Entity error
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct ErrorInfo {
    pub errors: HashMap<String, Vec<String>>,
}

#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct Balance {
    pub balance: i32,
}
#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct Trans {
    pub pro_id: i32,
    pub quantity: f32,
    pub unit: MeasureSend,
    pub end_price: i32,
}
#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct BalanceList {
    pub vec: Vec<Trans>,
    pub balance: Balance,
}
#[derive(Clone, PartialEq, Atom, Default)]
pub struct Token(pub String);

#[derive(Clone, PartialEq, Atom, Default)]
pub struct User(pub Option<UserSend>);

#[derive(Clone, PartialEq, Atom, Default)]
pub struct Db(pub Option<ProDb>);
//impl Default for User {
//    fn default() -> Self {
//        User(None)
//    }
//}
//impl Default for Db {
//    fn default() -> Self {
//        Db(None)
//    }
//}

#[derive(Clone, PartialEq, Deserialize, Debug)]
pub enum MeasureSend {
    Grams,
    Kilo,
    Count,
    Liters,
    HunGram,
}
#[derive(Clone, PartialEq, Deserialize, Debug)]
pub enum DataWrap {
    ProDb(ProDb),
    User(UserSend),
    Error,
}
#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct ProDb {
    pub vec: Vec<Produkt>,
}
#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct Produkt {
    pub id: i32,
    pub pro: String,
    pub image: Option<String>,
    pub desc: String,
    pub price: i32,
    pub stock: i32,
    pub mesure: MeasureSend,
}
#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct UserSend {
    pub id: i32,
    pub username: String,
    //list : Option<Vec<u8>>,
    pub balance: i32,
}

#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct Login {
    pub token: String,
    pub user: UserSend,
}
