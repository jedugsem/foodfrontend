use stylist::style;
use stylist::yew::styled_component;
use stylist::Style;
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement};
use yew::prelude::*;
use yew::html::NodeRef as Node;
use yew_hooks::{use_async, use_click_away};

use crate::funcs::*;

#[derive(Properties, PartialEq)]
pub struct ModalProps {
    pub children: Children,
    pub string: String,
    pub classes: Classes,
}

fn modal_style() -> (Style, Style) {
    (
        style!(
            "
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
"
        )
        .expect("eu"),
        style!(
            "
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 80%; /* Could be more or less, depending on screen size */
"
        )
        .expect("shat"),
    )
}

#[styled_component(Modal)]
pub fn modal(props: &ModalProps) -> Html {
    let (modal_class, modal_content_class) = modal_style();

    let modal : Node = use_node_ref();
    let modal_content : Node = use_node_ref();
    let button : Node = use_node_ref();
    let modal_i = use_state(|| false);
    {
        let modal_content = modal_content.clone();
        let modal = modal.clone();
        let modal_i = *modal_i.clone();
        use_click_away(modal_content.clone(), move |_: Event| {
            if modal_i {
                if let Some(modal) = modal.cast::<web_sys::HtmlElement>() {
                    modal.style().set_property("display", "none").unwrap();
                }
            }
        });
    }
    let onclick = {
        let modal = modal.clone();
        let modal_i = modal_i.clone();
        move |_| {
            if let Some(modal) = modal.cast::<web_sys::HtmlElement>() {
                modal.style().set_property("display", "block").unwrap();
                modal_i.set(true);
            }
        }
    };

    let onclick1 = {
        let modal = modal.clone();
        let modal_i = modal_i.clone();
        move |_| {
            if let Some(modal) = modal.cast::<web_sys::HtmlElement>() {
                modal.style().set_property("display", "none").unwrap();
                modal_i.set(false);
            }
        }
    };

    html!(
    <div>
        <button class={props.classes.clone()} ref={button} {onclick} >{ props.string.clone() }</button>
        <div ref={modal} class={modal_class}>
            <div ref={modal_content} class={modal_content_class}>
                <div class={css!("float : right;")}>
                    <button id="cm" onclick={onclick1}>{"X"}</button>
                </div>
                { for props.children.iter() }
            </div>
        </div>
    </div>
    )
}

#[function_component(Submit)]
pub fn submit() -> Html {
    let input_value_handle1 = use_state(String::default);
    //let input_handles = [use_state(String::default);6];

    let input_value1 = (*input_value_handle1).clone();
    let input_value_handle2 = use_state(String::default);
    let input_value2 = (*input_value_handle2).clone();
    let input_value_handle3 = use_state(String::default);
    let input_value3 = (*input_value_handle3).clone();
    let input_value_handle4 = use_state(String::default);
    let input_value4 = (*input_value_handle4).clone();
    let input_value_handle5 = use_state(String::default);
    let input_value5 = (*input_value_handle5).clone();
    let input_value_handle6 = use_state(String::default);
    let input_value6 = (*input_value_handle6).clone();

    let on_input1 = {
        let input_value_handle1 = input_value_handle1.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                input_value_handle1.set(input.value());
            }
        })
    };

    let on_input2 = {
        let input_value_handle2 = input_value_handle2.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                input_value_handle2.set(input.value());
            }
        })
    };

    let on_input3 = {
        let input_value_handle3 = input_value_handle3.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                input_value_handle3.set(input.value());
            }
        })
    };

    let on_input4 = {
        let input_value_handle4 = input_value_handle4.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                input_value_handle4.set(input.value());
            }
        })
    };

    let on_input5 = {
        let input_value_handle5 = input_value_handle5.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                input_value_handle5.set(input.value());
            }
        })
    };

    let on_input6 = {
        let input_value_handle6 = input_value_handle6.clone();
        Callback::from(move |e: InputEvent| {
            // When events are created the target is undefined, it's only
            // when dispatched does the target get added.
            let target: Option<EventTarget> = e.target();
            // Events can bubble so this listener might catch events from child
            // elements which are not of type HtmlInputElement
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(input) = input {
                input_value_handle6.set(input.value());
            }
        })
    };

    let submit = {
        let input_value1 = input_value1.clone();
        let input_value2 = input_value2.clone();
        let input_value3 = input_value3.clone();
        let input_value4 = input_value4.clone();
        let input_value5 = input_value5.clone();
        let input_value6 = input_value6.clone();
        //let input_value_handle1 = input_value_handle1;
        //let input_value_handle2 = input_value_handle2one();
        //let input_value_handle3 = input_value_handle3.clone();
        //let input_value_handle4 = input_value_handle4.clone();
        //let input_value_handle5 = input_value_handle5.clone();
        //let input_value_handle6 = input_value_handle6.clone();
        use_async(async move {
            post_submit(
                input_value1.clone(),
                input_value6.clone(),
                input_value4.clone(),
                input_value3.clone(),
                input_value2.clone(),
                input_value5.clone(),
                //                    name:input_value1,
                //                    image:input_value6,
                //                    price:input_value,
                //                    stock:input_value3,
                //                    desc:input_value2,
                //                    measure:input_value6,
            )
            .await
        })
    };

    let onclick = {
        let submit = submit.clone();

        Callback::from(move |_| {
            submit.run();
        })
    };
    if submit.data.is_some() {

        //input_value_handle1.clone().set("".to_string());
    }

    html!(
        <>
            <Modal string={"submit"} classes={classes!()}>
                    <p>
                    <label for="name">
                         { "Name :          " }
                          <input oninput={on_input1}
                             id="name"
                              type="text"
                              value={input_value1.clone()}
                          />

                  </label>
                  </p>
                    <p>
                    <label for="desc">
                         { "Description :   " }
                          <input oninput={on_input2}
                             id="desc"
                              type="text"
                              value={input_value2.clone()}
                          />

                  </label>
                  </p>
                    <p>
                    <label for="stock">
                         { "Stock :         " }
                          <input oninput={on_input3}
                             id="stock"
                              type="text"
                              value={input_value3.clone()}
                          />

                  </label>
                  </p>
                    <p>
                    <label for="price">
                         { "Price :         " }
                          <input oninput={on_input4}
                             id="price"
                              type="text"
                              value={input_value4.clone()}
                          />

                  </label>
                  </p>
                    <p>
                    <label for="unit">
                         { "Unit :          " }
                          <input oninput={on_input5}
                             id="unit"
                              type="text"
                              value={input_value5.clone()}
                          />

                  </label>
                  </p>
                    <p>
                    <label for="image">
                         { "Image :         " }
                          <input oninput={on_input6}
                             id="image"
                              type="text"
                              value={input_value6.clone()}
                          />

                  </label>
                  </p>
                <p>{if let Some(data) = &submit.data {
                    format!("{}",data)
                } else {
                    "worked".to_string()
                }}</p>
                <button onclick={onclick} ><p>{"submit"}</p></button>
            </Modal>
        </>
    )
}
